const app = getApp();
Page({
  options: {
    addGlobalClass: true,
  },
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    elements: [
      { title: 'LCD自定义', name: 'form', color: 'purple', icon: 'vipcard' },
      { title: 'LED自定义', name: 'nav', color: 'mauve', icon: 'formfill' },
      { title: 'LCD标准化', name: 'project', color: 'pink', icon: 'list' },
      { title: 'LED标准化', name: 'card', color: 'brown', icon: 'newsfill' },
      // { title: '表单', name: 'form', color: 'red', icon: 'formfill' },
      // { title: '时间轴', name: 'timeline', color: 'orange', icon: 'timefill' },
      // { title: '聊天', name: 'chat', color: 'green', icon: 'messagefill' },
      // { title: '轮播', name: 'swiper', color: 'olive', icon: 'album' },
      // { title: '模态框', name: 'modal', color: 'grey', icon: 'squarecheckfill' },
      // { title: '步骤条', name: 'steps', color: 'cyan', icon: 'roundcheckfill' },
    ],
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.name;
    if(id=="project"||id=="card"){
      wx.showModal({
        title: "信息安全提示",
        content:
          "标准化提供常见大屏标准配置报价，" +
          "请确认后点击【我已知晓】。",
        confirmText: "我已知晓",
        confirmColor: "#4B6DE9",
        cancelText: "我拒绝",
        success: result => {
          if (!result.confirm) {
           wx.navigateBack({
             delta: 0,
           })
          } else {
            wx.navigateTo({
              url: '/packageG/daping/' + id+'/'+id,
          });
          }
        }
      });
    }else{
      wx.navigateTo({
        url: '/packageG/daping/' + id+'/'+id,
    });
    }


  },
})
