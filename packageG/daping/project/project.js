const app = getApp();
Page({
  data: {
    title: '',
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    ergong:[],
    itemList:null
  },
  clickImg(event) {
    const imageUrl = event.currentTarget.dataset.url
    wx.previewImage({
      current: imageUrl,
      urls: [imageUrl]
    })
  },
  onLoad: function () {
    this.setData({
      userInfo:app.globalData.userInfo,
      ergong:app.globalData.ergong,
      itemList:app.globalData.itemList
    })
  },
  onUnload: function () {},

});