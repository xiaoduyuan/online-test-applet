const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    sudu:1,
    CustomBar: app.globalData.CustomBar,
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    video:null,
    flag:1
  },
    //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
    bindButtonRate (e) { // 设置倍速播放
      let rate = e.currentTarget.dataset.rate
      this.setData({
        flag:rate
      })
      this.setData({
        sudu:rate
      })
      this.videoContext.playbackRate(Number(rate))
  },
  onReady(e){
    this.videoContext=wx.createVideoContext('myVideo')
    // this.initAudio()
    // this.initRecord();
  },
    onLoad(options){
      // const id = e.currentTarget.dataset.id;
      console.log(options)
      app.formPost('/api/wx/student/video/select/' + options.id, null)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            video: res.response,
            isLoading:false
          })
          wx.showToast({
            title: '已扣除10积分！',
            icon: 'none',
            duration: 2000
            })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
    },

});