const app = getApp();
Page({
  data: {
    isLoading: true,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor1:'blue',//背景色
    color:'#fff',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    bgColor:'#f4f5f7',
    height:'30',
    top:0,
    bottom:0,
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    help_status:false,
    neirong: null,
    userInfo:null,
    queryParam: {
      pageIndex: "1",
      pageSize: "1000"
    },
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    material:null,
    materialList:[],
    modalName:null,
    total:0,
    gridCol:3,
    value:"",
    type: '全部',
    typeList:[{
      value:"ALL",
      title:"全部"
    },{
      value:"A",
      title:"泵房信息化"
    },{
      value:"B",
      title:"信息化硬件"
    },{
      value:"C",
      title:"拼接屏"
    },
    {
      value:"D",
      title:"表计"
    },
    {
      value:"E",
      title:"水联网"
    },
  ]
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  gridchange: function (e) {
    this.setData({
      gridCol: e.detail.value
    });
  },
  gridswitch: function (e) {
    this.setData({
      gridBorder: e.detail.value
    });
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '在线考试exam',
      path: '/page/index/index'
    }
  },
  onShareTimeline: function (t) {
    return {
      title: "在线考试exam",
      query: "",
      imageUrl: ""
    }
  },
    //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
    onLoad(){
      this.setData({
        userInfo: app.globalData.userInfo
      })
       this.search()
    },
    typeChanged: function (e) {
      const type = this.data.typeList[e.currentTarget.dataset.index].title;
      this.setData({ type: type});
      if(type=="全部"){
        this.setData({
          'queryParam.title': ""
        });
      }else{
        this.setData({
          'queryParam.title': type
        });
      }
      this.search()
    },
  search: function () {
    app.formPost('/api/wx/student/material/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          materialList: res.response.list,
          total:res.response.total,
          isLoading: false
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  },
  bindSearchInput: function (e) {
    this.setData({
      'queryParam.name': e.detail.value
    });
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        name: e.detail.value
      },
      value:e.detail.value
    })
  },
  tapHelp: function(e){
    if(e.target.id == 'help'){
      this.hideHelp();
    }
  },
  showHelp: function(e){
    this.setData({
      'help_status': true
    });
  },
  hideHelp: function(e){
    this.setData({
      'help_status': false
    });
  },
  menuBorder: function (e) {
    this.setData({
      menuBorder: e.detail.value
    });
  },
  menuArrow: function (e) {
    this.setData({
      menuArrow: e.detail.value
    });
  },
  menuCard: function (e) {
    this.setData({
      menuCard: e.detail.value
    });
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.showModal({
      title: "积分信息提示",
      content:
        "点击查看需要消耗您的积分，" +
        "请确认后点击【我已知晓】。",
      confirmText: "我已知晓",
      confirmColor: "#4B6DE9",
      cancelText: "我拒绝",
      success: result => {
        if (!result.confirm) {
         wx.navigateBack({
           delta: 0,
         })
        } else {
          wx.navigateTo({
            url: '../perdition/perdition?id=' + id,
        });
        }
      }
    });

  },
});