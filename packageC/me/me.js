const app = getApp();

Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        spinShow: false,
        photo:"",
        photoId:"",
        fengmianPic: "",
        urls:[],
        shuliang:"",
        queryParam: {
            pageIndex: "1",
            pageSize: "100",
            photoId:""
          },
        photoList:[],
        isCanDraw: !1,
        loadingshow: 0,
        hasMore: 0,
        id: "",
        StyleColor:"black",
        myCollectionId: "",
        myCollectionId2: "",
        telPermission: !1,
        isCheck: !1,
        isMe: !1,
        meAdmin: "",
        previewFlag: !1,
        give: !1,
        collection: !1,
        number: 0,
        windowW: "",
        windowH: "",
        headImg: "",
        imgList: [],
        logoAvartar: "",
        imagePath: ""
    },
    botBtn: function() {
        this.setData({
            isCanDraw: !this.data.isCanDraw
        });
    },
    onReachBottom: function() {
      // this.setData({
      //   'queryParam.pageIndex': this.data.queryParam.pageIndex + 4
      // }), 
      // app.formPost('/api/wx/student/photoList/pageList', _this.data.queryParam)
      // .then(res => {
      //   if (res.code == 1) {
      //     var newArr1 = [];
      //     console.log(newArr1)
      //     console.log(newArr1)
      //     for (let index = 0; index < res.response.total; index++) {
      //         newArr1.push(res.response.list[index].image);
      //     }   
      //     console.log(newArr1)
      //     _this.setData({
      //       spinShow: false,
      //       photoList: res.response.list,
      //       urls:newArr1
      //     })
      //   } else {
      //     wx.showModal({
      //       title: res.message
      //     })
      //   }
      // }).catch(e => {
      //   wx.showModal({
      //     title: e
      //   })
      // })
  },
  showColor: function(){
    this.setData({
      StyleColor: "red"
  });
  },
    mePreviewImage: function(e) {
        this.setData({
            previewFlag: !0
        });
        var t = e.currentTarget.dataset.url;
        console.log(t),       
        wx.previewImage({
            current: t,
            urls: this.data.urls
        });
    },
    onLoad:function(e){
        var id = e.id
        let _this = this
        _this.setData({
            spinShow: true,
            'queryParam.photoId':id
          });
        app.formPost('/api/wx/student/photo/select/' + id, null)
        .then(res => {
          if (res.code == 1) {
            _this.setData({
              photo: res.response
            })
            wx.setStorageSync("fengmianPic", res.response.url);
            wx.setStorageSync("signature", "        你在"+res.response.name+"看风景, 看风景人在楼上看你。 明月装饰了你的窗子, 你装饰了别人的梦。");
          } else {
            wx.showModal({
              title: res.message
            })
          }
        }).catch(e => {
          wx.showModal({
            title: e
          })
        })
        app.formPost('/api/wx/student/photoList/pageList', _this.data.queryParam)
        .then(res => {
          if (res.code == 1) {
            var newArr1 = [];
            console.log(newArr1)
            console.log(newArr1)
            for (let index = 0; index < res.response.total; index++) {
                newArr1.push(res.response.list[index].image);
            }   
            console.log(newArr1)
            _this.setData({
              spinShow: false,
              photoList: res.response.list,
              urls:newArr1,
              shuliang:res.response.total
            })
          } else {
            wx.showModal({
              title: res.message
            })
          }
        }).catch(e => {
          wx.showModal({
            title: e
          })
        })
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onfresh();
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});