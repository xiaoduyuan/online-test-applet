const app = getApp();
Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        userArr: [],
        maxlim: 10,
        pageindex: 0,
        hidden: !1,
        scrollTop: 0,
        loadingshow: 0,
        hasMore: 0,
        photoList:[],
        shuLiang:0,
        queryParam: {
            pageIndex: "1",
            pageSize: "100",
            title:""
          },
    },
    onLoad: function() {
         this.search()
    },
    bindSearchInput: function (e) {
      this.setData({
        'queryParam.title': e.detail.value
      });
      this.setData({
        queryParam: {
          pageIndex: 1,
          pageSize: app.globalData.pageSize,
          title: e.detail.value
        },
      })
    },
    search: function () {
        app.formPost('/api/wx/student/photo/pageList', this.data.queryParam)
        .then(res => {
          if (res.code == 1) {
            this.setData({
              photoList: res.response.list,
              shuLiang:res.response.total
            })
          } else {
            wx.showModal({
              title: res.message
            })
          }
        }).catch(e => {
          wx.showModal({
            title: e
          })
        })
      },
    pageBack: function() {
        wx.navigateBack({
            delta: 1
        });
    },
    goDetail: function(t) {
        var id =  t.currentTarget.dataset.id;
        wx.navigateTo({
            url: "/packageC/me/me?id=" + id
        });
    },
    // getfriendlist: (o = (0, s.default)(i.default.mark(function t() {
    //     var e, a = this;
    //     return i.default.wrap(function(t) {
    //         for (;;) switch (t.prev = t.next) {
    //           case 0:
    //             return e = this, t.abrupt("return", new Promise(function(t, n) {
    //                 e.setData({
    //                     loadingshow: 1,
    //                     hasMore: 1
    //                 }), u.collection("friend").limit(e.data.maxlim).get().then(function(n) {
    //                     console.log(n.data), t(n.data), e.setData({
    //                         loadingshow: 0,
    //                         hasMore: 0
    //                     }), a.setData({
    //                         userArr: n.data
    //                     });
    //                 }).catch(function(t) {
    //                     n(t), wx.showToast({
    //                         title: "网络可能出错",
    //                         icon: "none",
    //                         mask: !0
    //                     });
    //                 });
    //             }));

    //           case 2:
    //           case "end":
    //             return t.stop();
    //         }
    //     }, t, this);
    // })), function() {
    //     return o.apply(this, arguments);
    // }),
    // getmorefriendlist: (n = (0, s.default)(i.default.mark(function t() {
    //     var e, a = this;
    //     return i.default.wrap(function(t) {
    //         for (;;) switch (t.prev = t.next) {
    //           case 0:
    //             return e = this, t.abrupt("return", new Promise(function(t, n) {
    //                 e.setData({
    //                     hasMore: 1,
    //                     loadingshow: 1
    //                 }), u.collection("friend").skip(e.data.maxlim * e.data.pageindex).limit(e.data.maxlim).get().then(function(n) {
    //                     console.log(n.data), t(n.data), 0 == n.data.length ? a.setData({
    //                         hasMore: 0
    //                     }) : a.setData({
    //                         userArr: e.data.userArr.concat(n.data)
    //                     });
    //                 }).catch(function(t) {
    //                     n(t), wx.showToast({
    //                         title: "网络可能出错",
    //                         icon: "none",
    //                         mask: !0
    //                     });
    //                 });
    //             }));

    //           case 2:
    //           case "end":
    //             return t.stop();
    //         }
    //     }, t, this);
    // })), function() {
    //     return n.apply(this, arguments);
    // }),
    // onfresh: (a = (0, s.default)(i.default.mark(function t() {
    //     var e;
    //     return i.default.wrap(function(t) {
    //         for (;;) switch (t.prev = t.next) {
    //           case 0:
    //             return e = this, wx.showNavigationBarLoading(), wx.showLoading({
    //                 title: "刷新中..."
    //             }), t.next = 5, e.getfriendlist().then(function(t) {
    //                 wx.hideLoading(), wx.hideNavigationBarLoading(), wx.stopPullDownRefresh();
    //             });

    //           case 5:
    //           case "end":
    //             return t.stop();
    //         }
    //     }, t, this);
    // })), function() {
    //     return a.apply(this, arguments);
    // }),
    // submit: (e = (0, s.default)(i.default.mark(function t(e) {
    //     var a, n;
    //     return i.default.wrap(function(t) {
    //         for (;;) switch (t.prev = t.next) {
    //           case 0:
    //             if (a = this, n = e.detail.value.content, console.log(n), "" != n) {
    //                 t.next = 7;
    //                 break;
    //             }
    //             wx.showToast({
    //                 title: "请输入搜索内容",
    //                 icon: "none"
    //             }), t.next = 8;
    //             break;

    //           case 7:
    //             return t.abrupt("return", new Promise(function(t, e) {
    //                 wx.showLoading({
    //                     title: "加载中"
    //                 }), setTimeout(function() {
    //                     wx.hideLoading();
    //                 }, 6e3), u.collection("friend").where({
    //                     name: u.RegExp({
    //                         regexp: n,
    //                         options: "i"
    //                     })
    //                 }).get().then(function(e) {
    //                     console.log(e.data), t(e.data), wx.hideLoading({}), 0 == e.data.length && wx.showToast({
    //                         title: "暂无数据",
    //                         icon: "none"
    //                     }), a.setData({
    //                         userArr: e.data
    //                     });
    //                 }).catch(function(t) {
    //                     e(t), wx.showToast({
    //                         title: "网络可能出错",
    //                         icon: "none",
    //                         mask: !0
    //                     });
    //                 });
    //             }));

    //           case 8:
    //           case "end":
    //             return t.stop();
    //         }
    //     }, t, this);
    // })), function(t) {
    //     return e.apply(this, arguments);
    // }),
    // onLoad: (t = (0, s.default)(i.default.mark(function t(e) {
    //     var a;
    //     return i.default.wrap(function(t) {
    //         for (;;) switch (t.prev = t.next) {
    //           case 0:
    //             return a = this, getApp().SetColor(a), t.next = 4, a.getfriendlist().then(function(t) {});

    //           case 4:
    //           case "end":
    //             return t.stop();
    //         }
    //     }, t, this);
    // })), function(e) {
    //     return t.apply(this, arguments);
    // }),
 
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onfresh();
    },
    // onReachBottom: function() {
    //     this.setData({
    //       'queryParam.pageIndex': this.data.queryParam.pageIndex + 4
    //     }), this.search();
    // },
    onShareAppMessage: function() {}
});