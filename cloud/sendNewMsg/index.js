// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    const result = await cloud.openapi.subscribeMessage.send({
      touser: event.openid ,
      page: 'pages/index/index',
      lang: 'zh_CN',
      data: {
        thing10: {
          value: event.theme
        },
        number4: {
          value: event.number
        },
        thing1: {
          value: event.title
        },
        thing6: {
          value: event.bukao
        },
        name7: {
          value: event.username
        }
      },
      templateId: 'Q5B4N5l0cmyoNYC7siPyF7rv-xN6aT8FV3jWqUCRQ2Y',
      miniprogramState: 'developer'
    })
    console.log(result)
    return result
  } catch (err) {
    console.log(err)
    return err
  }
}