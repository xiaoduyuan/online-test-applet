Component({
    properties: {
        openSettingDialog: Boolean,
        content: String
    },
    data: {
        openSettingDialog: !1
    },
    methods: {
        closeOpenSetting: function() {
            this.setData({
                openSettingDialog: !1
            });
        }
    }
});