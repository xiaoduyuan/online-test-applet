# MP CU

本项目为 `colorui3.x` 微信小程序原生版。

`colorui3.x` 默认只支持 `uni-app`，本项目中 `colorui` 框架为移植修改版。

在线文档地址：[http://mp.color-ui.com/](http://mp.color-ui.com/)

本项目地址：[https://github.com/Color-UI/MP-CU](https://github.com/Color-UI/MP-CU)

`colorui3.x` 地址： [https://github.com/weilanwl/coloruiBeta](https://github.com/Color-UI/MP-CU)

