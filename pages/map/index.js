const app = getApp();
Page({
  data: {
    latitude: 30.562474,
    longitude: 114.351725,
    mapHeight: 0,
    typeTabTop: 0,
    markers: []
  },
  onLoad: function (e) {
    let _this = this
      app.formPost('/api/wx/student/exampaper/answer/address', null).then(res => {
        if (res.code === 1) {
          _this.setData({
            markers: res.response
          });
        }
      }).catch(e => {
        app.message(e, 'error')
      })
      // _this.mapCtx = wx.createMapContext('myMap',_this)
  },
  tapLocation(event) {

    this.primayMapCtx.moveToLocation({
      success: function (res) {
        console.log('movetolocation success')
      },
      fail: function (res) {
        console.log('movetoloction fail ' + res.errMsg)
      }
    })
  },
  onReady: function (e) {
    // this.mapCtx = wx.createMapContext('myMap')
    this.primayMapCtx = wx.createMapContext('myMap')
  },
})