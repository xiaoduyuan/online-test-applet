//login.js
//获取应用实例
var app = getApp();
var Mcaptcha = require('../../../utils/mcaptcha.js');
Page({
  data: {
    remind: '加载中',
    help_status: false,
    userid_focus: false,
    passwd_focus: false,
    code_focus: false,
    userName: '',
    password: '',
    codeImg: '',
    angle: 0
  },
  onReady: function(){
    var _this = this;
    setTimeout(function(){
      _this.setData({
        remind: ''
      });
    }, 100);
    wx.onAccelerometerChange(function(res) {
      var angle = -(res.x*30).toFixed(1);
      if(angle>14){ angle=14; }
      else if(angle<-14){ angle=-14; }
      if(_this.data.angle !== angle){
        _this.setData({
          angle: angle
        });
      }
    });
    _this.mcaptcha = new Mcaptcha({
      el: 'canvas',
      width: 80,
      height: 35,
      createCodeImg: ""
    });
  },
    //刷新验证码
    onTap() {
      this.mcaptcha.refresh();
    },
      //验证码
  inputChange(e) {
    console.log(e.detail.value)
    this.setData({
      imgCode: e.detail.value
    })
  },
  showResetModal(e) {
    wx.navigateTo({
      url: '/pages/user/register/index',
    })
  },
  formSubmit: function (e) {
    let _this = this
    //验证验证码
    var res = _this.mcaptcha.validate(_this.data.imgCode);
    if (_this.data.imgCode == '' || _this.data.imgCode == null) {
      wx.showModal({
        title: '请输入图形验证码'
      })
      return;
    }
    if (!res) {
      wx.showModal({
        title: '图形验证码错误'
      })
      _this.onTap()
      return;
    };
    wx.login({
      success(wxres) {
        if (wxres.code) {
          console.log(e.detail)
          e.detail.value.code = wxres.code
          app.formPost('/api/wx/student/auth/bind', e.detail.value)
            .then(res => {
              if (res.code == 1) {
                wx.setStorageSync('token', res.response)
                wx.reLaunch({
                  url: '/pages/index/index',
                });
              } else {
                wx.showModal({
                  title: res.message
                })
              }
            }).catch(e => {
              wx.showModal({
                title: e
              })
            })
        } else {
          wx.showModal({
            title: res.errMsg
          })
        }
      }
    })
  },
  useridInput: function(e) {
    this.setData({
      userName: e.detail.value
    });
    if(e.detail.value.length >= 12){
      wx.hideKeyboard();
    }
  },

  passwdInput: function(e) {
    this.setData({
      passwd: e.detail.value
    });
    if(e.detail.value.length >= 18){
      wx.hideKeyboard();
    }
  },
  codeInput: function(e) {
    this.setData({
      code: e.detail.value
    });
    if(e.detail.value.length >= 4){
      wx.hideKeyboard();
    }
  },
  inputFocus: function(e){
    if(e.target.id == 'userName'){
      this.setData({
        'userid_focus': true
      });
    }else if(e.target.id == 'password'){
      this.setData({
        'passwd_focus': true
      });
    }else if(e.target.id == 'codeImg'){
      this.setData({
        'code_focus': true
      });
    }
  },
  inputBlur: function(e){
    if(e.target.id == 'userName'){
      this.setData({
        'userid_focus': false
      });
    }else if(e.target.id == 'password'){
      this.setData({
        'passwd_focus': false
      });
    }else if(e.target.id == 'codeImg'){
      this.setData({
        'code_focus': false
      });
    }
  },
  tapHelp: function(e){
    if(e.target.id == 'help'){
      this.hideHelp();
    }
  },
  showHelp: function(e){
    this.setData({
      'help_status': true
    });
  },
  hideHelp: function(e){
    this.setData({
      'help_status': false
    });
  },
  protocol: function() {
    wx.navigateTo({
      url: '../about/about'
    })
  },
});