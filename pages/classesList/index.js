const app = getApp();
Page({
  data: {
    spinShow: false,
    loadMoreLoad: false,
    loadMoreTip: '暂无数据',
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    modalName:null,
    avatar: [
      'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
      'https://ossweb-img.qq.com/images/lol/web201310/skin/big81005.jpg',
      'https://ossweb-img.qq.com/images/lol/web201310/skin/big25002.jpg',
      'https://ossweb-img.qq.com/images/lol/web201310/skin/big91012.jpg'
    ],
    queryParam: {
      classesId: null,
      password: null
    },
    inputValue:null,
    list: [],
    queryParam: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize
    }
  },
    toChild(e) {
      wx.navigateTo({
        url: e.currentTarget.dataset.url
      })
    },
  onLoad() {
    let _this = this;
    _this.setData({
      spinShow: true
    });
    app.formPost('/api/wx/student/classes/pageListAll', _this.data.queryParam).then(res => {
      _this.setData({
        spinShow: false
      });
      if (res.code == 1) {
        if(res.response.total>0){
        _this.setData({
          spinShow: false,
          list: res.response.list,
          loading: false
        })
        }else{
          _this.setData({
            loadMoreLoad: false,
            loadMoreTip: '暂无数据'
          });
        }
      }else{
        _this.setData({
          loadMoreLoad: false,
          loadMoreTip: '暂无数据'
        });
      }
      console.log(res)
    })
  },
  showModal(e) {
    let abc= e.currentTarget.dataset.abc
    console.log("value："+abc)
    this.setData({
      modalName:"DialogModal1",
      ['queryParam.classesId']:e.currentTarget.dataset.abc
    })
  },
  showModal2(e) {
    let abc= e.currentTarget.dataset.abc
    console.log("value："+abc)
wx.navigateTo({
  url: "/pages/classes/index?classesId="+abc,
})
  },
  hideModal(e) {
    console.log(this.data.inputValue)
   //查询密码是否正确，并且加入班级 
   this.setData({
    ['queryParam.password']:this.data.inputValue
  })
  console.log(this.data.queryParam)
   app.formPost('/api/wx/student/classesUser/into',  this.data.queryParam).then(res => {
     if(res.code==1){
      this.setData({
        modalName: null,
      })
      
      wx.showModal({
        title: '提示',
        content: "恭喜你，成功找到组织！！！"
      })
     }else{
      wx.showModal({
        title: '提示',
        content: res.message
      })
     }
   })
    
  },
  hideModal1(e) {
    this.setData({
      modalName: null
    })
  },
  bindinput: function (e) {
    // console.log(e.detail.value)
    this.setData({
      inputValue: e.detail.value
    })
  },

});