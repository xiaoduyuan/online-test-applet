let app = getApp()
var Utils = require('../../utils/util'); //相对路径
Page({
  data: {
    painting: {},
    shareImage: '',
    title: null,
    realName: null,
    isPass: "很遗憾你未通过",
    tishi1: null,
    createTime: null,
    paperScore: null,
    modalName: null,
    address: null,
    id: 0,
    percent: 0,
    userScore: null
  },
  onLoad: function (options) {
    let id = options.id
    let _this = this
    _this.setData({
      spinShow: true,
      id: options.id
    });
    app.formPost('/api/wx/student/exampaper/answer/result/' + _this.data.id, null)
      .then(res => {
        _this.setData({
          spinShow: false
        });
        if (res.code === 1) {
          _this.setData({
            title: res.response.paperName,
            realName: app.globalData.userInfo.realName,
            createTime: res.response.createTime,
            paperScore: res.response.paperScore,
            address: res.response.address,
            userScore: res.response.userScore
          });
          if (_this.data.userScore >= _this.data.paperScore * 0.6) {
            wx.showLoading({
              title: '绘制证书中',
              mask: true
            })
              //加密证书编号
              let jiamiqian=app.globalData.userInfo.userName + ";" + res.response.userScore+";"+res.response.createTime;
              let zhengshubianhao = Utils.encrypt(jiamiqian); //文件名.方法名  加密
  
            this.setData({
              painting: {
                width: 555,
                height: 375,
                clear: true,
                views: [{
                    type: 'image',
                    url: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/certificate.png?sign=8255c42bc070e82e25dca05873add4bc&t=1612019178',
                    top: 0,
                    left: 0,
                    width: 555,
                    height: 375
                  },
                  {
                    type: 'image',
                    url: '../../images/gongzhang.png',
                    top: 270,
                    left: 370,
                    width: 55,
                    height: 55
                  },
                  {
                    type: 'text',
                    content: app.globalData.userInfo.realName,
                    fontSize: 16,
                    color: '#402D16',
                    textAlign: 'left',
                    top: 170,
                    left: 250,
                    bolder: true
                  },
                  {
                    type: 'text',
                    content: "恭喜您已通过《" + res.response.paperName + "》考试",
                    fontSize: 18,
                    color: '#402D16',
                    textAlign: 'left',
                    top: 210,
                    left: 96,
                    bolder: true
                  },
                  {
                    type: 'text',
                    content: res.response.createTime,
                    fontSize: 13,
                    color: '#563D20',
                    textAlign: 'left',
                    top: 300,
                    left: 96
                  },
                  {
                    type: 'text',
                    content: app.globalData.userInfo.userLevelName + "  " + app.globalData.userInfo.userDeptName,
                    fontSize: 13,
                    color: '#563D20',
                    textAlign: 'left',
                    top: 300,
                    left: 320
                  },
                  {
                    type: 'text',
                    content: "证书编号：" + zhengshubianhao,
                    fontSize: 10,
                    color: '#563D20',
                    textAlign: 'left',
                    top: 330,
                    left: 70
                  }
                ]
              }
            })
          } else {
            wx.vibrateShort({
              type: 'heavy'
            })
            wx.showModal({
              title: '提示',
              content: '很遗憾，您没有通过考试！',
              cancelText: "返回首页",
              confirmText: "返回列表",
              success(res) {
                if (res.confirm) {
                  wx.reLaunch({
                    url: "/pages/home/index"
                  });
                } else if (res.cancel) {
                  wx.reLaunch({
                    url: "/pages/index/index"
                  });
                }
              }
            })
          }
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        app.message(e, 'error')
      })
  },
  onShow: function () {},
  onShareAppMessage: function () {},
  toIndex: function () {
    wx.reLaunch({
      url: "/pages/index/index"
    });
  },
  eventSave() {
    wx.saveImageToPhotosAlbum({
      filePath: this.data.shareImage,
      success(res) {
        wx.showToast({
          title: '保存证书成功',
          icon: 'success',
          duration: 2000
        })
        wx.reLaunch({
          url: "/pages/home/index"
        });
      }
    })
  },
  eventGetImage(event) {
    console.log(event)
    wx.hideLoading()
    const {
      tempFilePath,
      errMsg
    } = event.detail
    if (errMsg === 'canvasdrawer:ok') {
      this.setData({
        shareImage: tempFilePath
      })
    }
  }
});