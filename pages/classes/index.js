const app = getApp();
Page({
  data: {
    spinShow: false,
    loadMoreLoad: false,
    loadMoreTip: '暂无数据',
    userName:"",
    classesName: null,
    classesNumber: null,
    img: 'https://image.weilanwl.com/color2.0/plugin/wdh2236.jpg',
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    queryParam: {
      pageIndex: "1",
      pageSize: "100",
      classesId: null
    },
    tableData: [],
    showbutton:true
  },
  onLoad: function (options) {
    let _this = this;

    console.log("春过来的ID" + options.classesId)
    let id = parseInt(options.classesId)
    _this.setData({
      spinShow: true,
      ['queryParam.classesId']: id,
      userName:app.globalData.userInfo.userName
    });
    app.formPost('/api/wx/student/classes/select/' + id, null).then(e => {
      _this.setData({
        classesName: e.response.name,
        classesNumber: e.response.classesNumber
      });
    })
    app.formPost('/api/wx/student/classesUser/page', _this.data.queryParam).then(res => {
      _this.setData({
        spinShow: false
      });
      if (res.code == 1) {
        _this.setData({
          tableData: res.response.list
        });
        if(res.response.list.length==0){
          _this.setData({
            showbutton:false
          });
        }
      } else {
        _this.setData({
          loadMoreLoad: false,
          loadMoreTip: '暂无数据'
        });
      }
      console.log(res)
    })
  },
  hideModal() {

    app.formPost('/api/wx/student/classesUser/delete/'+this.data.queryParam.classesId, null).then(res => {
      if (res.code == 1) {
        wx.showModal({
          title: '提示',
          content: "你已成功退出班级！",
          success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/classesList/index',
              })
            } else if (res.cancel) {
              wx.navigateTo({
                url: '/pages/classesList/index',
              })
            }
          }
        })
       
      } else {
        wx.showModal({
          title: '提示',
          content: res.message
        })
      }
    })
  },
});