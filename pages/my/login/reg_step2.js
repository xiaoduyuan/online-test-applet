
const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
    StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		userInfo:app.globalData.userInfo,
		userInfo:app.globalData.userInfo,
		canIUseGetUserProfile: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {
	},
	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
	},
	getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善个人资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
					'userInfo.imagePath':res.userInfo.avatarUrl,
				})
				app.formPost('/api/wx/student/user/update', this.data.userInfo)
				.then(res => {
					if (res.code == 1) {
						app.globalData.userInfo=this.data.userInfo;
						wx.reLaunch({
							url: '/pages/index/index',
						})
					} else {
						wx.showModal({
							title: '更新用户信息失败',
						})
						wx.reLaunch({
							url: '/pages/index/index',
						})
					}
				})
			}
    })
	},
	getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    this.setData({
      'userInfo.imagePath':res.userInfo.avatarUrl
		})
		app.formPost('/api/wx/student/user/update', this.data.userInfo)
		.then(res => {
			if (res.code == 1) {
				app.globalData.userInfo=this.data.userInfo;
				wx.reLaunch({
					url: '/pages/index/index',
				})
			} else {
				wx.showModal({
					title: '更新用户信息失败',
				})
				wx.reLaunch({
					url: '/pages/index/index',
				})
			}
		})
  },
	url: function (e) {
		ccminiPageHelper.url(e, this);
	} 
})