const app = getApp()
const {
  $Toast
} = require('../../../dist/base/index');
var Utils = require('../../../utils/util'); //相对路径
Page({
  data: {
    bottom: 100,
    bgColor2: "#5677fc",
    btnList: [{
      bgColor: "#16C2C2",
      //名称
      text: "搜索",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#64B532",
      //名称
      text: "关于",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#FFA000",
      //名称
      text: "通知",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }],
    showActionSheet: false, //控制显隐
    tips: "确认清空搜索历史吗？", //提示文本
    itemList: [{
      text: "退出登录",
      color: "#e53a37"
    }],
    color: "#9a9a9a",
    size: 26,
    isCancel: true, //是否取消
    maskClosable: true, //mask是否可以点击
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    spinShow: false,
    logoutModalVisible: false,
    info: {},
    count: 0,
    canvasWidth: "",
    canvasHeight: "",
    canvasLeft: "",
    canvasTop: "",
    newUser:"",
    modalName: null,
    userinfomation: null,
    showhaibao: false, //隐藏显示
    maskHidden: true, //隐藏显示
    modalName1: null,
    picName: '流星之夜',
    signed:false,
    pic: [{
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg1.jpeg',
        name: '春天'
      },
      {
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg2.jpeg',
        name: '夏天'
      },
      {
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg3.jpeg',
        name: '秋天'
      },
      {
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg4.jpeg',
        name: '冬天'
      },
      {
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg5.jpeg',
        name: '幽静'
      },
      {
        link: 'https://cdn.zhoukaiwen.com/zjx_me_bg6.jpg',
        name: '天空'
      }
    ],
    topBackGroupImageIndex: 5,
    inter: [{
        title: 'mimicry',
        name: '活力春天',
        color: ''
      },
      {
        title: 'theme',
        name: '清爽夏日',
        color: ''
      },
      {
        title: 'theme',
        name: '金秋之韵',
        color: ''
      },
      {
        title: 'theme',
        name: '冬日之阳',
        color: ''
      },
      {
        title: 'theme',
        name: '幽兰星空',
        color: ''
      },
      {
        title: 'theme',
        name: '流星之夜',
        color: ''
      }
    ],
    showAppreciate: false,
    appreciateSelectIndex:0,
    modalName:null
  },

  onClick(e) {
    let index = e.detail.index
    switch (index) {
      case -1:
        // util.toast("您点击了悬浮按钮")
        break;
      case 0:
        wx.navigateTo({
          url: "/packageH/search/search"
        })
        break;
      case 1:
        wx.navigateTo({
          url: "/pages/my/aboutus/aboutus"
        })
        break;
      case 2:
        wx.navigateTo({
          url: "/pages/notice/notice"
        })
        break;
      default:
        break;
    }
  },
  switchImage: function switchImage(e) {
    console.log(e.currentTarget.dataset.id)
    console.log(e.currentTarget.dataset.name)
    wx.setStorageSync('topBackGroupImageIndex', e.currentTarget.dataset.id)
    wx.setStorageSync('picName', e.currentTarget.dataset.name)
    this.setData({
      modalName1: null,
      topBackGroupImageIndex: e.currentTarget.dataset.id,
      picName: e.currentTarget.dataset.name
    })
  },
  gotoMy:function(){
    wx.navigateTo({
      url: '/pages/my/about/about',
    })
  },
  jifen:function(){
    wx.navigateTo({
      url: '/pages/information/information',
    })
  },
  bangzhu:function(){
    wx.navigateTo({
      url: '/pages/my/help/help',
    })
  },
	bindSetTap: async function (e) {
		wx.showActionSheet({
			itemList: ['清除缓存', '退出登录'],
			success: async res => {
				let idx = res.tapIndex;
				if (idx == 0) {
          wx.clearStorage()
          this.closeActionSheet();
				}
				if (idx == 1) {
          app.formPost('/api/wx/student/auth/unBind', null).then(res => {
            if (res.code == 1) {
              wx.setStorageSync('token', '')
              this.closeActionSheet();
              wx.reLaunch({
                url: '/pages/user/bind/index',
              });
            }
            this.setData({
              spinShow: false
            });
          }).catch(e => {
            this.setData({
              spinShow: false
            });
            app.message(e, 'error')
          })
				}
			},
			fail: function (res) {}
		})
	},

  houtai(){

  },
  navProcess12(e) {

    wx.navigateTo({
      url: '/pages/user-list/user-list',
    });
  },
  showModal: function showModal(e) {
    this.setData({
      modalName1: "Modal"
    })
  },
  // 弹窗
  showAppreciateModal: function (e) {
    this.setData({
      showAppreciate: true
    })
  },
  closeAppreciateModal: function (e) {
    this.setData({
      showAppreciate: false
    })
  },
        appreciateSelect(e) {
          this.setData({
            appreciateSelectIndex: e.currentTarget.dataset.index
          })
        },
  deterAppreciateSelect: function () {
    this.closeAppreciateModal();
  },
  onLoad: function (options) {
    wx.getStorageSync({
      key: 'topBackGroupImageIndex',
      success: function (res) {
        console.log(res)
        this.setData({
          topBackGroupImageIndex: res.data
        });
      }
    })
    wx.getStorageSync({
      key: 'picName',
      success: function (res) {
        console.log(res)
        this.setData({
          picName: res.data
        });
      }
    })

    this.setData({
      spinShow: true,
      info: app.globalData.userInfo,
      spaceShow: true
    });
    this.loadUserInfo()
    this.setData({
      spinShow: false
    });
  },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  onReady: function () {

    // this.create();
    //创建初始化图片

  },
  hideModal: function () {
    this.setData({
      logoutModalVisible: false
    })
  },
  onShow: function () {
    this.setData({
      maskHidden: true,
      showhaibao: false
    })
  },
  loadUserInfo() {
    let _this = this
    _this.setData({
      spinShow: true
    });
  },
  // 只允许从相机扫码
  subMsg() {
    let that = this;
    wx.scanCode({
      onlyFromCamera: true,
      success: function (res) {
        console.log("扫描了" + res.result)
        try {
          app.globalData.decParam = Utils.decrypt(res.result); //解密
          that.setData({
            modalName: 'Modal',
            userinfomation: app.globalData.decParam
          })
          if(that.data.info.role==3){
          app.formPost('/api/wx/student/user/select/'+ app.globalData.userInfo.id, null)
          .then(res => {
            if (res.code == 1) {
              that.setData({
                newUser:res.response,
                'newUser.integral':res.response.integral+100,
              })
              app.formPost('/api/wx/student/user/updateIntegral', that.data.newUser).then(
                res1 => {
                  if (res1.code == 1) {
                    wx.showModal({
                      title: '新增100积分成功！',
                    })
                  }
                })
            } else {
              wx.showModal({
                title: '新增积分失败！',
              })
            }
          })
        }else{
          wx.showModal({
            title: '用户无权限！',
          })
        }
        } catch (err) {
          console.log(err)
          wx.showModal({
            title: '提示',
            content: '非法二维码：校验信息有误！'
          })
        }
      }
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  closeActionSheet() {
    this.setData({
      showActionSheet: false
    })
  },
  // 点击
  itemClick: function (e) {
    this.closeActionSheet();
    app.formPost('/api/wx/student/auth/unBind', null).then(res => {
      if (res.code == 1) {
        wx.setStorageSync('token', '')
        wx.setStorageSync('isFirst', false)
        wx.reLaunch({
          url: '/pages/user/bind/index',
        });
      }
      this.setData({
        spinShow: false
      });
    }).catch(e => {
      this.setData({
        spinShow: false
      });
      app.message(e, 'error')
    })
  },
  gotoUrl(){
    wx.navigateTo({
      url: '/pages/my/memberCode/memberCode',
    })
  },
  jifen(){
    wx.navigateTo({
      url: '/pages/information/information',
    })
  },
  logOut() {
    let _this = this
    wx.clearStorage()
    
    _this.setData({
      // spinShow: true,
      showActionSheet: true
    });

  },
  showMask: function (e) {
    this.selectComponent("#authorCardId").showMask()
  },
  clickHandler() {
    this.setData({
      count: this.data.count + 1
    });
  },
  clickIng(e) {
    let that = this
    this.zan = setInterval(function () {
      that.setData({
        count: that.data.count + 1
      });
    }, 100)
  },
  clickCancel() {
    clearInterval(this.zan)
  },
  // 震动
  vibrateLongTap: function () {
    // 使手机振动400ms
    wx.vibrateLong();
  },

  goHelp: function () {
    // 使手机振动400ms
   wx.navigateTo({
     url: '../help/help',
   })
  },
  vibrateShortTap: function () {
    // 使手机振动15ms
    wx.vibrateShort();
  },
  share: function (e) {
    $Toast({
      content: '图片生成中...',
      type: 'loading',
      duration: 0
    });
    var _this = this
    setTimeout(function () {
      _this.create();
      _this.setData({
        maskHidden: false,
        showhaibao: true
      })
      $Toast.hide();
    }, 2000)
  },
  //创建
  getImage: function (url) {
    return new Promise((resolve, reject) => {
      wx.getImageInfo({
        src: url,
        success: function (res) {
          resolve(res)
        },
        fail: function () {
          reject("")
        }
      })
    })
  },
  getImageAll: function (image_src) {
    let that = this;
    var all = [];
    image_src.map(function (item) {
      all.push(that.getImage(item))
    })
    return Promise.all(all)
  },
  //保存
  save: function () {
    var that = this;
    //canvas 生成图片 生成临时路径
    wx.canvasToTempFilePath({
      canvasId: 'canvas',
      success: function (res) {
        console.log(res.tempFilePath);
        var tempFilePath = res.tempFilePath;
        that.setData({
          imagePath: tempFilePath
        });

      }
    })
  },
  gotoSubmit: function (e) {
    $Toast({
      content: '图片生成中...',
      type: 'loading',
      duration: 0
    });
    var _this = this
    setTimeout(function () {
      _this.create();
      _this.setData({
        maskHidden: false,
        showhaibao: true
      })
      $Toast.hide();
    }, 2000)

  },
  //点击图片进行预览，长按保存分享图片
  previewImg: function (e) {
    var img = this.data.imagePath
    wx.previewImage({
      current: img, // 当前显示图片的http链接
      urls: [img] // 需要预览的图片http链接列表
    })
  },
  savelocalImg: function () {
    var _this = this
    wx.saveImageToPhotosAlbum({
      //下载图片
      filePath: _this.data.imagePath,
      success: function () {
        wx.showToast({
          title: "保存成功",
          icon: "success",
        })
        _this.setData({
          maskHidden: false,
          showhaibao: true
        })
      }
    });
  },
  onPullDownRefresh() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    setTimeout(function () {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      //停止当前页面下拉刷新。
      wx.stopPullDownRefresh()
    }, 1000)
  }
})