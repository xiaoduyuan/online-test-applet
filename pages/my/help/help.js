var app = getApp();

Page({
      /**
       * 页面的初始数据
       */
      data: {
            StatusBar: app.globalData.StatusBar,
            CustomBar: app.globalData.CustomBar,
            list: [{
                  title: '该程序是做什么的？',
                  id: 0,
                  des: ['本程序主要是方便华中大区同事学习、考试。',
                        '如果您有相关想法意见，可以点击右下角联系客服。'
                  ],
                  check: true,
            }, {
                  title: '该程序收费吗？',
                  id: 1,
                  des: ['本程序是完全的公益项目，永久承诺不收取任何费用，您可以随心所欲的使用。'],
                  check: false,
            }, {
                  title: '为什么要留下联系方式？',
                  id: 2,
                  des: ['本程序交易完全由大笨笨一号一人开发。', '由于精力有限，难免会有部分bug。预留联系方式是为了方便您反馈提交相关bug信息。'],
                  check: false,
            }, {
                  title: '关于资料下载问题',
                  id: 3,
                  des: ['对于本小程序中的资料，做出以下申明', '1、所有资料都存储在本人的私有服务器中，不存储在任何第三方系统中。', '2、资料下载是需要具备权限，权限不足无法进行资料下载。', '3、本小程序不面向所有人开放。', '4、本小程序下载需要使用积分。', '注册账号是需要我本人进行审核，并且小程序中存在严格的日志记录。'],
                  check: false,
            }, {
                  title: '小程序能使用多久？',
                  id: 4,
                  des: ['如本小程序后台使用的是阿里云服务器，租赁期限是截止2022年11月13日。此时间前都可正常使用，后续到期再根据实际情况调整。', '由于经费问题，租赁的阿里云带宽为2Mbps,下载速度偏慢，后期将提供阿里云盘下载链接。'],
                  check: false,
            }, {
                  title: '本程序的通知形式？',
                  id: 5,
                  des: ['本小程序在进入时，会提示您订阅成绩。','当您点击订阅成绩后，','后续的考试成绩以及相关考试通知都会以微信订阅消息的方式通知您。'],
                  check: false,
            }, ]
      },
      onReady() {},

      show(e) {
            var that = this;
            let ite = e.currentTarget.dataset.show;
            let list = that.data.list;
            if (!ite.check) {
                  list[ite.id].check = true;
            } else {
                  list[ite.id].check = false;
            }
            that.setData({
                  list: list
            })
      },
      //跳转页面
      go(e) {
            wx.navigateTo({
                  url: e.currentTarget.dataset.go
            })
      },
      onLoad() {

      },

})