// pages/books/portrait.js
var app = getApp()
const audioCtx = '';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    indexBgImage: 'https://www.dabenben.cn:9001/dma/110.jpg',
    coverBgImage: 'https://www.dabenben.cn:9001/dma/111.jpg',
    fromBgImage:  'https://www.dabenben.cn:9001/dma/112.jpg',
    book1BgImage:  'https://www.dabenben.cn:9001/dma/113.jpg',
    book2BgImage:  'https://www.dabenben.cn:9001/dma/114.jpg',
    score1BgImage: 'https://www.dabenben.cn:9001/dma/115.jpg',
    score2BgImage:  'https://www.dabenben.cn:9001/dma/116.jpg',
    endBgImage:  'https://www.dabenben.cn:9001/dma/117.jpg',
    deviceInfo: '',
    fullscreen: '',
    contentStyle: 'width:320px;height:500px;',
    showTips: true,
    hasLogin: false,
    hasNeedData: false,
    sharerid: false,
    data: '',
    // isLoading:true,
    userInfo:"",
    kaoshicishu:0,
    zuigaofen:0,
    kaoshifenshu:0,
    kaishiweizhi:"",
    kaoshimingcheng:"",
    peixuncishu:0,
    times:0,
    fristTime:0,
    denglucishu:0,
    times1:0,
    fristTime1:0,
    baiwenbaida:0,
    times2:0,
    fristTime2:0,
    peixunshipin:0,
    times3:0,
    fristTime3:0,
    kaoshifuxi:0,
    times4:0,
    fristTime4:0,
    wodexiangce:0,
    times5:0,
    fristTime5:0,
    fristTimeContent:"",
    fristTimeContent2:"",
    fristTimeContent3:"",
    fristTimeContent4:"",
    fristTimeContent5:"",
    LastTimeContent5:"",
    queryParam: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      paperName:""
    },
    queryParam1: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"培训材料"
    },
    queryParam2: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"在线考试"
    },
    queryParam3: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"百问百答"
    },
    queryParam4: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"培训视频"
    },
    queryParam5: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"培训文档"
    },
    queryParam6: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"我的相册"
    },
  },

  onShow () {
    // this.audioCtx = wx.createAudioContext('greetings');
    // this.audioCtx.play()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {
   	// 当页面影藏时，暂停音乐
    this.audioCtx.pause()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.inital(options)
  },
  inital: function (options) {
    const sysinfo = wx.getSystemInfoSync()
    const educookie = app.globalData.edusysUserInfo ? app.globalData.edusysUserInfo.cookie : ''
    const opaccookie = app.globalData.opacLogin ? app.globalData.opacLogin.cookie : ''
    let hasNeedData = false
    if (educookie != '' && opaccookie != '') {
      hasNeedData = true
    }
    this.setData({ 
      deviceInfo: sysinfo, 
      fullscreen: `width:${sysinfo.screenWidth}px;height:${sysinfo.screenHeight-30}px;`,
      hasNeedData: hasNeedData
    })

    const sharerid = options.sharerid ? options.sharerid : false
    if (sharerid) {
      this.setData({ sharerid: sharerid })
      this.getData(sharerid, '1', '1', sharerid, '1', '1')
    }
    
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  startAction: function () {
    wx.showLoading({
      title: '正在生成中...',
    })
    let _this = this
    // setTimeout(() => {
    //     if (_this.data.loading === 'auto') {
    //         _this.setData({isLoading: false})
    //     }
    // }, 800);
    app.formPost('/api/wx/student/exampaper/answer/pageList',_this.data.queryParam).then(res => {
      if (res.code === 1) {
        _this.setData({
          kaoshicishu:res.response.total,
          userInfo:app.globalData.userInfo
        });
      }
    })
    app.formPost('/api/wx/student/exampaper/answer/max',null).then(res => {
      if (res.code === 1) {
        _this.setData({
          zuigaofen:res.response.total,
          kaoshifenshu:res.response.userScore*0.1,
          kaishiweizhi:res.response.address,
          kaoshimingcheng:res.response.paperName
        });
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam1).then(res => {
      if (res.code === 1) {
        _this.setData({
          peixuncishu:res.response.total,
         
        });
        if(res.response.total>0){
          _this.setData({
            times:res.response.list[0].createTime,
          fristTime:res.response.list[res.response.list.length-1].createTime,
          fristTimeContent:res.response.list[res.response.list.length-1].content,
        });
        }
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam2).then(res => {
      if (res.code === 1) {
        _this.setData({
          denglucishu:res.response.total,
         
        });

      }
      if(res.response.total>0){
        _this.setData({
          times1:res.response.list[0].createTime,
          fristTime1:res.response.list[res.response.list.length-1].createTime
      });
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam3).then(res => {
      if (res.code === 1) {
        _this.setData({
          baiwenbaida:res.response.total,
        
        });
        if(res.response.total>0){
          _this.setData({
            times2:res.response.list[0].createTime,
            fristTime2:res.response.list[res.response.list.length-1].createTime,
            fristTimeContent2:res.response.list[res.response.list.length-1].content,
        });
        }

      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam4).then(res => {
      if (res.code === 1) {
        _this.setData({
          peixunshipin:res.response.total,
         
        });
        if(res.response.total>0){
          _this.setData({
            times3:res.response.list[0].createTime,
            fristTime3:res.response.list[res.response.list.length-1].createTime,
            fristTimeContent3:res.response.list[res.response.list.length-1].content,
            LastTimeContent3:res.response.list[0].content,
        });
      }

      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam5).then(res => {
      if (res.code === 1) {
        _this.setData({
          kaoshifuxi:res.response.total,
         
        });
        if(res.response.total>0){
          _this.setData({
          times4:res.response.list[0].createTime,
          fristTime4:res.response.list[res.response.list.length-1].createTime,
          fristTimeContent4:res.response.list[res.response.list.length-1].content,
        });
        }
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam6).then(res => {
      if (res.code === 1) {
        _this.setData({
          wodexiangce:res.response.total,
         
        });
        if(res.response.total>0){
          _this.setData({
            times5:res.response.list[0].createTime,
          fristTime5:res.response.list[res.response.list.length-1].createTime,
          fristTimeContent5:res.response.list[res.response.list.length-1].content,
          LastTimeContent5:res.response.list[0].content,
        });
        }
      }
    })
    setTimeout(function () {
      wx.hideLoading()
      _this.setData({
        hasLogin: true
      });
      }, 2000)   
  },
  checkNeedData: function () {
    const eduid = app.globalData.edusysUserInfo.uid
    const edupass = app.globalData.edusysUserInfo.password
    const educookie = app.globalData.edusysUserInfo ? app.globalData.edusysUserInfo.cookie : ''
    const opacid = app.globalData.opacLogin ? app.globalData.opacLogin.uid : ''
    const opacpass = app.globalData.opacLogin ? app.globalData.opacLogin.pwd : ''
    const opaccookie = app.globalData.opacLogin ? app.globalData.opacLogin.cookie : ''

    return true
  },
  swiperChange: function(e) {
    const currentIndex = e.detail.current
    if (currentIndex == 6) {
      this.setData({ showTips: false })
    } else {
      this.setData({ showTips: true })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const nickname = app.globalData.userInfo.nickName
    const shareTitle = this.data.data ? `在线考试 - ${nickname}的个人画像` : '在线考试 - 个人画像生成'
    return {
      title: shareTitle,
      path: `pages/portrait/portrait`
    }
  }
})