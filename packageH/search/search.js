
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
		StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    height:'30',
    top:0,
    bottom:0,
    baseConfig: [{
      icon: '../images/icon-action.png',
      title: '培训资料搜索',
      navigateMark: '/packageH/searchAll/searchAll'
    },
    {
      icon: '../images/icon-basic.png',
      title: '学习视频搜索',
      navigateMark: '/packageH/searchVideo/searchVideo'
    },
    {
      icon: '../images/icon-flex.png',
      title: '百问百答搜索',
      navigateMark: '/packageG/wenDa/wenDa'
    },
    {
      icon: '../images/icon-navigator.png',
      title: '考试成绩搜索',
      navigateMark: '/packageF/search/search'
    },
    {
      icon: '../images/icon-view.png',
      title: '萌新资料搜索',
      navigateMark: '/packageH/searchMX/searchMX'
    },
    // {
    //   icon: '../images/icon-shop.png',
    //   title: '物料中心搜索',
    //   navigateMark: '/packageH/searchWL/searchWL'
    // },
    {
      icon: '../images/icon-form.png',
      title: '演示demo搜索',
      navigateMark: '/packageH/searchYS/searchYS'
    },
    // {
    //   icon: '../images/icon-form.png',
    //   title: '软件资质搜索',
    //   navigateMark: '/packageH/searchZZ/searchZZ'
    // }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },
  onNaviCard:function(e){
    console.log(e.currentTarget.dataset.navigatemark)
    let url=e.currentTarget.dataset.navigatemark;
    wx.navigateTo({
      url: url,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})