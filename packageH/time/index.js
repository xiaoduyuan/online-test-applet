//index.js
const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
  },

  onLoad: function () {
   
   this.setTimeRunner()
  },

  setTimeRunner: function () {
    let that = this;
    this.timeRunner = setInterval(function () {
      that.getTimeStr();
      that.timeArr();
    }, 1000);
  },

   getTimeStr: function () {
    let that = this;
    var time = new Date();
    var hour = ('00' + time.getHours()).slice(-2);
    var minute = ('00' + time.getMinutes()).slice(-2);
    var second = ('00' + time.getSeconds()).slice(-2);
    var timeStr = (hour + minute + second).split('');
    that.setData({timeStr:timeStr})
  },

    timeArr: function () {
      let that = this;
      var timeArr = this.data.timeStr.map(function (unit, index) {
        var max;
        if (index & 1 == 1) { 
          max = 9;
        } else if (index == 0) { 
          max = 2;
        } else if (index == 2) { 
          max = 5;
        } else if (index == 4) { 
          max = 5;
        }
        var current = Number(unit)
        that.setData({
          max: max,
          current: current,
        })
        return {
          max: max,
          current: current
        };
      })
      that.setData({timeArr:timeArr})
    },


    beforeDestroy:function () {
      clearInterval(this.timeRunner);
    },



  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    this.beforeDestroy()
    // 页面关闭
  },
  onPullDownRefresh: function(){
    wx.stopPullDownRefresh()
  },
})