// packageF/baobiao/baobiao.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
		StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    height:'30',
    top:0,
    bottom:0,
    elements: [{
      title: 'F2图表示例',
      name: 'chart',
      color: 'cyan',
      icon: 'newsfill',
      url:'/packageF/chart/chart'
    },
    {
      title: '考试报表',
      name: 'exam',
      color: 'blue',
      icon: 'form',
      url:'/packageF/baobiao/kaoshi/index'
    },
    {
      title: '学习报表',
      name: 'learning',
      color: 'purple',
      icon: 'font',
      url:''
    },
    {
      title: '下载报表 ',
      name: 'material',
      color: 'mauve',
      icon: 'icon',
      url:''
    },
    {
      title: '其他报表',
      name: 'other',
      color: 'pink',
      icon: 'btn',
      url:''
    }
  ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})