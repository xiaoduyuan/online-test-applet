

const app = getApp();

Page({

  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    showHint: false,
    canVerify: false,
    identity: { studentId: "", oaSecret: "" }
  },


  toggleHint() {
    const showHint = !this.data.showHint;
    this.setData({ showHint });
    showHint && ( // 如果是在展开而不是收起提示
        () => setTimeout( // 那么展开动画结束后，将页面滚动至提示最下方
          () => wx.pageScrollTo({ selector: ".hint-last", duration: 100 }), 201
        )
      )
  },

  onLoad() {


  },
  verify(){
    wx.navigateTo({
      url: '/packageF/examPapers/index',
    })
  },

  onReady() {
    wx.getStorage({
      key: 'signPrivacyConfirm',
      success (res) {
        console.log(res.data)
      },
      fail(res){
        wx.showModal({
          title: "隐私信息提示",
          content:
            "小程序部分功能（如考试、下载资料）需要验证并" +
            "使用您的身份信息，以提供功能或保证信息安全。",
          confirmText: "我已知晓",
          confirmColor: "#4B6DE9",
          cancelText: "我拒绝",
          success: result => {
            if (!result.confirm) {
             wx.navigateBack({
               delta: 0,
             })
            } else {
              wx.setStorageSync("signPrivacyConfirm", true);
            }
          }
        });
      }
    })
  },

  // onShow() {}

})
