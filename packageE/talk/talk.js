const app = getApp();
var t, e, a, n
// new Date();
Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        tabList: [ "广场", "我的", "喜欢" ],
        TabCur: 0,
        scrollLeft: 0,
        selected: 0,
        cardCur: 0,
        skip: 0,
        loadingshow: 0,
        hasMore: 0,
        postData: {
            public: !0,
            textlen: 0,
            bgColor: "#FFF",
            font: "f",
            face: 4
        },
        typing: !0,
        showTextarea: !1,
        news: "",
        mynews: "",
        mylike: ""
    },
    tabSelect: function(t) {
        this.setData({
            TabCur: t.currentTarget.dataset.id,
            scrollLeft: 60 * (t.currentTarget.dataset.id - 1)
        }), this.onRefresh(t.currentTarget.dataset.id);
    },
    cardSwiper: function(t) {
        this.setData({
            cardCur: t.detail.current
        }), t.detail.current == this.data.news.length - 1 && (console.log("已经到了最后一个啦~"), 
        this.getmore());
    },
    getmore: function() {
        var t = this;
        t.setData({
            loadingshow: 1,
            hasMore: 1
        }), this.setData({
            skip: t.data.skip + 20
        }), wx.cloud.callFunction({
            name: "talkground",
            data: {
                action: "get",
                skip: t.data.skip
            },
            success: function(e) {
                console.log(e.result.data);
                var a = e.result.data;
                for (var n in a) a[n].likeuser.indexOf(t.data.openid) > -1 ? a[n].isLike = !0 : a[n].isLike = !1;
                t.setData({
                    news: t.data.news.concat(a)
                }), e.result.data.length < 1 && t.setData({
                    hasMore: 0
                });
            },
            fail: function() {
                console.error, wx.showToast({
                    title: "出错啦~",
                    icon: "none"
                });
            }
        });
    },
    fire: function() {
        this.pub(), this.setData({
            selected: 1
        });
    },
    nlp: function(t) {
        var e = t.replace(/\s+/g, ""), a = e.length;
        a > 100 ? wx.showToast({
            title: "字太多了"
        }) : this.setData({
            typing: !1,
            "postData.textlen": a
        }), console.log(e + a);
    },
    prevent: function() {},
    pub: function() {
        this.setData({
            is_published: !0
        }), this.animate(".add-post", [ {
            opacity: 0,
            bottom: 0,
            width: "100%",
            height: 0,
            transformOrigin: "50% 50%",
            backgroundColor: "#ccc"
        }, {
            width: "100%",
            height: "60%",
            ease: "ease-in",
            opacity: 1,
            backgroundColor: "#FFF"
        } ], 200, function() {
            this.setData({
                showTextarea: !0
            }), this.clearAnimation(".add-post", {
                opacity: !0
            }, function() {});
        }.bind(this));
    },
    close: function() {
        var t = this;
        this.setData({
            showTextarea: !1
        }), this.animate(".modal", [ {
            width: "100%",
            height: "0%"
        } ]), this.animate(".add-post", [ {
            width: "100%",
            height: "60%",
            backgroundColor: "#FFF"
        }, {
            width: "100%",
            height: "0%",
            ease: "ease-in",
            opacity: 0,
            backgroundColor: "#FFF"
        } ], 200, function() {
            this.clearAnimation(".add-post", {
                opacity: !0
            }, function() {
                t.setData({
                    is_published: !1
                });
            });
        }.bind(this));
    },
    postSave: function(t) {
        var e, a, n = this, o = t.detail.value.posttext;
        o.length < 5 ? wx.showToast({
            title: "小确幸不能少于五个字喔",
            icon: "none"
        }) : (wx.showLoading({
            title: "努力发布中",
            icon: "loading",
            mask: !0
        }), this.data.userAuth && (n.data.postData.postText = o, n.data.postData.saveType = "text", 
        wx.cloud.callFunction({
            name: "talkground",
            data: {
                action: "add",
                time: (e = new Date().getTime(), a = new Date(e), "".concat(a.getFullYear(), "-").concat(a.getMonth() + 1, "-").concat(a.getDate(), " ").concat(a.getHours(), ":").concat(a.getMinutes())),
                text: n.data.postData,
                userinfo: n.data.userData
            }
        }).then(function(t) {
            console.log(t), 87014 != t.result.errCode ? wx.showToast({
                title: "发布成功",
                icon: "success",
                mask: !0,
                success: function() {
                    n.close(), n.onRefresh(0);
                }
            }) : wx.showToast({
                title: "发布失败，含有违规内容",
                icon: "none"
            });
        }).catch(function(t) {
            console.error(t), wx.showToast({
                title: "发布失败，请稍后重试",
                icon: "none"
            });
        })));
    },
    like: function(t) {
        var e = t.currentTarget.dataset.id, a = t.currentTarget.dataset.like, n = t.currentTarget.dataset.index;
    },
    deletedeatil: function(t) {
        console.log(t.currentTarget.dataset.id);
        var e = this;
        wx.showModal({
            title: "提示",
            content: "确定要删除该小确幸吗？",
            success: function(a) {
                if (a.confirm) {
                    var n = t.currentTarget.dataset.id;
                    wx.showToast({
                        title: "被你喜欢也是一种小确幸~",
                        icon: "none",
                        mask: !0
                    }), wx.cloud.callFunction({
                        name: "talkground",
                        data: {
                            _id: n,
                            action: "delete"
                        },
                        success: function(a) {
                            var n = t.currentTarget.dataset.index, o = e.data.mynews;
                            o.splice(n, 1), e.setData({
                                mynews: o
                            });
                        },
                        fail: function(t) {
                            wx.showToast({
                                title: "失败啦！",
                                icon: "none",
                                mask: !0
                            }), console.error("删除失败", t);
                        }
                    });
                } else if (a.cancel) return !1;
            }
        });
    },
    dislikedeatil: function(t) {
        console.log(t.currentTarget.dataset.id);
        var e = this;
        wx.showModal({
            title: "提示",
            content: "确定不喜欢我了嘛~",
            success: function(a) {
                if (a.confirm) {
                    var n = t.currentTarget.dataset.id;
                    wx.showToast({
                        title: "被你喜欢过，也是小确幸~",
                        icon: "none"
                    }), wx.cloud.callFunction({
                        name: "talkground",
                        data: {
                            action: "dislike",
                            id: n
                        },
                        success: function(a) {
                            console.log(a);
                            var n = t.currentTarget.dataset.index, o = e.data.mylike;
                            o.splice(n, 1), e.setData({
                                mylike: o
                            });
                        },
                        fail: function(t) {
                            wx.showToast({
                                icon: "none",
                                title: "取消点赞失败"
                            }), console.error("[云函数]  调用失败：", t);
                        }
                    });
                } else if (a.cancel) return !1;
            }
        });
    },
    onRefresh: function t(e) {

    },

    onLoad: function(t) {
        getApp().SetColor(this), this.onRefresh(0);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onRefresh(this.data.TabCur);
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});